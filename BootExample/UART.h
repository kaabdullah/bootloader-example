/*
******************************************
******************************************
**                UART.h                **
**            Date: 2/18/2017           **
**      MCU: AVR ATmega32/ATmega32L     **
**         IDE: Atmel Studio 6.2        **
**           Compiler: AVR-GCC          **
**       Author: Mohamed M. Hesham      **
**   E-mail: moh.moh.hesham@gmail.com   **
******************************************
******************************************
*/
//////////////////////////////////////////
/*
******************************************
******************************************
**        HEADER_FILE_PROTECTION        **
******************************************
******************************************
*/
#ifndef UART_H_
#define UART_H_
//////////////////////////////////////////
//////////////////////////////////////////

#include "COMMON_CONFIG.h"

/*
******************************************
******************************************
**             UART__MACROS             **
******************************************
******************************************
*/
#define NORMAL_SPEED					(0<<1)
#define DOUBLE_SPEED					(1U<<1)

#define DATA_LENGTH_5_BITS              (0<<2)|(0<<1)
#define DATA_LENGTH_6_BITS              (0<<2)|(1U<<1)
#define DATA_LENGTH_7_BITS              (1U<<2)|(0<<1)
#define DATA_LENGTH_8_BITS              (1U<<2)|(1U<<1)

#define ONE_STOP_BIT                    (0<<3)
#define TWO_STOP_BITS                   (1U<<3)

#define EVEN_PARITY                     (1U<<5)|(0<<4)
#define ODD_PARITY                      (1U<<5)|(1U<<4)
#define	NO_PARITY                       (0<<5)|(0<<4)
//////////////////////////////////////////
//////////////////////////////////////////
//pointer to function for callback
typedef void (*UARTp2F)(void);
#define NULL    0
/*
******************************************
******************************************
**            UART__CHANNELS            **
******************************************
******************************************
*/
typedef enum
{
	ASYNCHRONOUS ,
	SYNCHRONOIS_MASTER_RISING_CLK_POL ,
	SYNCHRONOIS_MASTER_FALLING_CLK_POL ,
	SYNCHRONOIS_SLAVE_RISING_CLK_POL ,
	SYNCHRONOIS_SLAVE_FALLING_CLK_POL
}UART_MODE_t;
//////////////////////////////////////////
//////////////////////////////////////////

/*
******************************************
******************************************
**              UART_MODES              **
******************************************
******************************************
*/
typedef enum
{
	POLLING,
	INTERRUPT_ON_RECEIVE,
	INTERRUPT_ON_TRANSMIT,
	INTERRUPT_ON_RECEIVE_TRANSMIT
}BEHAVIOR_MODE_t;
//////////////////////////////////////////
//////////////////////////////////////////
/*
******************************************
******************************************
**       UART_INITIATE_PARAMETERS       **
******************************************
******************************************
*/
typedef struct
{
	UART_MODE_t			UART_MODE;
	BEHAVIOR_MODE_t		BEHAVIOR_MODE;
	uint16_t	        BAUD_RATE;
	uint8_t				SPEED;
	uint8_t				DATA_LENGTH;
	uint8_t				STOP_BITS;
	uint8_t				PARITY;
}UART_INIT_PARAM_t;
//////////////////////////////////////////
//////////////////////////////////////////

/*
******************************************
******************************************
**       UART_FUNCTIONS_PROTOTYPE       **
******************************************
******************************************
*/
void vUARTInit(UART_INIT_PARAM_t *uart_parameters,UARTp2F Recieve);
uint8_t u8tUARTRx(void);
void vUARTTxByte(uint8_t data);
void vUARTTxString(char string[]);
//////////////////////////////////////////
#endif /* UART_H_ */
//////////////////////////////////////////