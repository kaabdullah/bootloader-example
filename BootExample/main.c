/*
 * BootExample.c
 *
 * Created: 2/18/2017 9:09:33 PM
 * compiler: AVR-GCC
 * IDE : ATMEL STUDIO 7 
 * Author : Kareem A.Abdullah
 * Mail : Kareem.A.Abdullah@outlook.com
 */ 
 
 #include "main.h"
 #include "UART.h"
 #include "FlashDriver.h"

 uint8_t g_FlashPageBuffer[SPM_PAGESIZE];
 volatile uint8_t g_FlashBufferIndex = 0;
 volatile uint8_t g_IsFlashPageBufferFull = FASLE;
 volatile uint8_t g_BuzzerSize;
 UART_INIT_PARAM_t UART_PARAM = {ASYNCHRONOUS,INTERRUPT_ON_RECEIVE,9600,NORMAL_SPEED,DATA_LENGTH_8_BITS,ONE_STOP_BIT,NO_PARITY};
 int main(void)
 {
	 vUARTInit(&UART_PARAM,vRecievedBufferHandler);
	 
	 sei(); //set enable interrupts
	 
	 vUARTTxString("Enter some characters and press Enter\r\n");
	 
	 while(1)
	 {
		 if (g_IsFlashPageBufferFull == TRUE)
		 {
			 g_IsFlashPageBufferFull = FASLE;
			 vFlashWritePage(FLASH_PAGE,g_FlashPageBuffer);
			 
			 vUARTTxString("\r\nPage Received from UART:");
			 vUARTTxString((char *)g_FlashPageBuffer);
			 vUARTTxString("\r\n");
			 
			 vBufferClear(g_FlashPageBuffer,g_BuzzerSize);
			 
			 vFlashReadPage(FLASH_PAGE, g_FlashPageBuffer);
			 
			 vUARTTxString("\r\nPage Read From Flash:");
			 vUARTTxString((char *)g_FlashPageBuffer);
			 vUARTTxString("\r\n");
			 
			 vBufferClear(g_FlashPageBuffer,g_BuzzerSize);
		 }
	 }
 }
 
 
 //recieve buffer Handler for UART call-back
void vRecievedBufferHandler(void)
 {
	 uint8_t data;
	 data = UDR;
	 
	 //entering the terminating character or reaching the page size, triggers flash write.
	 if ((data == TERMINATING_CHARACTER) || (g_FlashBufferIndex >= (SPM_PAGESIZE - 1)))
	 {
		 g_IsFlashPageBufferFull = TRUE;
		 g_BuzzerSize = g_FlashBufferIndex;
		 g_FlashBufferIndex = 0;
	 }
	 else
	 {
		 g_FlashPageBuffer[g_FlashBufferIndex++] = data;
		 //vUARTTxByte(data);
	 }
 }
 