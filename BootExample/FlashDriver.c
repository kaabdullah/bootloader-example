/*
 * FlashDriver.c
 *
 * Created: 2/18/2017 9:11:28 PM
 * compiler: AVR-GCC
 * IDE : ATMEL STUDIO 7
 * Author : Kareem A.Abdullah
 * Mail : Kareem.A.Abdullah@outlook.com
 */

 #include "FlashDriver.h"
 
 
 //*****************************************************************************
 //
 //! vBootProgramPage.
 //!
 //! \param uint32_t PageStartAddress which defines the flash page -to be written to - start address.
 //! \param uint8_t *buffer  which defines the starting address of the buffer to be written.
 //!
 //!  This is the main writing function for flash memory. Takes  a page start address and
 //!  a pointer to a buffer. Function MUST BE stored in the boot-loader section
 //!  of memory to enable proper functionality No Read While Write(NRWW) memory section.
 //!  If the buffer is greater than SPM_PAGESIZE, the remainder data
 //!  is discarded.
 //!  
 //!
 //! \return None.
 //
 //*****************************************************************************
 void vBootProgramPage (uint32_t PageStartAddress, uint8_t buffer[])
 {
	 uint8_t i;
	 uint8_t sreg;
	 uint16_t LittleEndianWord;
	 sreg = SREG;							//saving the value of SREG 
	 cli();									// Disable the global interrupt.
	 vEEPROMBusyWait();
	 VFlashPageErase (PageStartAddress);
	 vStoreProgramMemoryBusyWait();			// Wait until the memory is erased.
	 
	 for (i=0; i<SPM_PAGESIZE; i+=2)
	 {
		 // Set up little-endian word.
		 LittleEndianWord = *buffer++;
		 LittleEndianWord |= (*buffer++) << 8;
		 
		 vFlashPageFill ((PageStartAddress + (uint16_t)i), LittleEndianWord);
	 }
	 
	 VFlashPageWrite (PageStartAddress);     // Store buffer in flash page.
	 vStoreProgramMemoryBusyWait();					// Wait until the memory is written.
	 // Re-enable Read While Write-section again.
	 VRWWEnable ();
	 // Re-enable interrupts
	 sei();
	//restor the SREG value
	 SREG = sreg;
 }

 //*****************************************************************************
 //
 //! vFlashWritePage.
 //!
 //! \param uint32_t FlashPageNumber which defines the flash page -to be written to - Number.
 //! \param uint8_t *buffer  which defines the starting address of the buffer to be written.
 //!
 //!  This is a Wrapper function for boot_program_page. it Allows user to input a
 //!  page number instead of an address for simplicity of use. Requires
 //!  page number and a pointer to a buffer 
 //!  
 //!
 //! \return None.
 //
 //*****************************************************************************
 void vFlashWritePage(uint8_t FlashPageNumber, uint8_t buffer[])
 {
	 uint32_t PageAddress = FlashPageNumber*SPM_PAGESIZE;
	 vBootProgramPage(PageAddress,buffer);
 }

 //*****************************************************************************
 //
 //! cFlashReadByte.
 //!
 //! \param uint32_t address which defines the flash address to be read from.
 //!
 //! Read a character from flash memory pointed to by the given address 
 //!  
 //!
 //! \return char ReadValue.
 //
 //*****************************************************************************
 char cFlashReadByte (uint32_t address)
 {
	char ReadValue = pgm_read_byte(address);
	return ReadValue;
 }

 //*****************************************************************************
 //
 //! vFlashReadPage.
 //!
 //! \param uint32_t FlashPageNumber which defines the flash page -to be read from - Number.
 //! \param uint8_t *buffer  which defines the starting address of the buffer to be read to.
 //!
 //!
 //! This function Reads a page from the flash memory to the passed buffer.
 //! Functionality not defined if buffer is shorter than length
 //! SPM_PAGESIZE 
 //!  
 //!
 //! \return None.
 //
 //*****************************************************************************
 void vFlashReadPage (uint8_t FlashPageNumber, uint8_t buffer[])
 {
	 uint8_t i=0;
	 uint32_t PageAddress = (FlashPageNumber*SPM_PAGESIZE);
	
		for (i=0; i <SPM_PAGESIZE; i++)
		{
			*buffer = cFlashReadByte(PageAddress);
			vStoreProgramMemoryBusyWait();
			PageAddress++;
			buffer++;
		}
 }
 //*****************************************************************************
 //
 //! vBufferClear.
 //!
 //! \param uint8_t *buffer  which defines the starting address of the buffer to be earsed .
 //! \param uint8_t BufferSize which defines size to be earsed.
 //!
 //! This function clear a specific buffer
 //!  
 //!
 //! \return None.
 //
 //*****************************************************************************
 void vBufferClear(uint8_t buffer[],uint8_t BufferSize)
 {
  uint8_t i;
	 for(i=0;i<=BufferSize;i++)
	 {
		 buffer[i] = 0;
	 }
 }