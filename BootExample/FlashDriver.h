/*
 * FlashDriver.h
 *
 * Created: 2/18/2017 9:11:11 PM
 * compiler: AVR-GCC
 * IDE : ATMEL STUDIO 7
 * Author : Kareem A.Abdullah
 * Mail : Kareem.A.Abdullah@outlook.com
 */


#ifndef FLASHDRIVER_H_
#define FLASHDRIVER_H_

#include <avr/io.h>
#include <avr/boot.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

//wrapper macros for boot.h
#define vEEPROMBusyWait()					eeprom_busy_wait ()
#define	vStoreProgramMemoryBusyWait()		boot_spm_busy_wait ()
#define vFlashPageFill(ADDRESS,WORD)		boot_page_fill (ADDRESS,WORD)
#define VFlashPageErase(START_ADDRESS)		boot_page_erase(START_ADDRESS)
#define VFlashPageWrite(START_ADDRESS)		boot_page_write(START_ADDRESS)
#define VRWWEnable()						boot_rww_enable()

//function prototypes

void vBootProgramPage (uint32_t PageStartAddress, uint8_t buffer[]) BOOTLOADER_SECTION;
void vFlashWritePage(uint8_t FlashPageNumber, uint8_t buffer[]);
char cFlashReadByte (uint32_t address);
void vFlashReadPage (uint8_t FlashPageNumber, uint8_t buffer[]);
void vBufferClear(uint8_t buffer[],uint8_t BufferSize);

#endif /* FLASHDRIVER_H_ */