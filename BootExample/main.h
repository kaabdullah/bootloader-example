/*
 * main.h
 *
 * Created: 2/18/2017 10:18:47 PM
 * compiler: AVR-GCC
 * IDE : ATMEL STUDIO 7
 * Author : Kareem A.Abdullah
 * Mail : Kareem.A.Abdullah@outlook.com
 */ 


#ifndef MAIN_H_
#define MAIN_H_

#define F_CPU 8000000UL

#include <avr/io.h>
#include <stdint.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>




#define FASLE		0
#define TRUE		1

#define FLASH_PAGE	50

#define  TERMINATING_CHARACTER			'\r'


void vRecievedBufferHandler(void);

#endif /* MAIN_H_ */