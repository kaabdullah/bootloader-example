/*
******************************************
******************************************
**             UART_CONFIG.h            **
**            Date: 2/18/2017           **
**      MCU: AVR ATmega32/ATmega32L     **
**         IDE: Atmel Studio 6.2        **
**           Compiler: AVR-GCC          **
**       Author: Mohamed M. Hesham      **
**   E-mail: moh.moh.hesham@gmail.com   **
******************************************
******************************************
*/
//////////////////////////////////////////
/*
******************************************
******************************************
**        HEADER_FILE_PROTECTION        **
******************************************
******************************************
*/
#ifndef UART_CONFIG_H_
#define UART_CONFIG_H_
//////////////////////////////////////////
//////////////////////////////////////////
#include "UART.h"
//////////////////////////////////////////
//////////////////////////////////////////
/*
******************************************
******************************************
**      PARAMETERS_CHOOSING_STRUCT      **
******************************************
******************************************
*/
UART_INIT_PARAM_t UART_PARAM = {ASYNCHRONOUS,INTERRUPT_ON_TRANSMIT,9600,NORMAL_SPEED,DATA_LENGTH_8_BITS,ONE_STOP_BIT,NO_PARITY};
//////////////////////////////////////////
//////////////////////////////////////////
//////////////////////////////////////////
#endif /* UART_CONFIG_H_ */
//////////////////////////////////////////								