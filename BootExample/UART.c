/*
******************************************
******************************************
**                UART.c                **
**            Date: 2/18/2017           **
**      MCU: AVR ATmega32/ATmega32L     **
**         IDE: Atmel Studio 6.2        **
**           Compiler: AVR-GCC          **
**       Author: Mohamed M. Hesham      **
**   E-mail: moh.moh.hesham@gmail.com   **
******************************************
******************************************
*/
//////////////////////////////////////////
/*
******************************************
******************************************
**        HEADER_FILE_INCLUSION         **
******************************************
******************************************
*/
#include "UART.h"

static UARTp2F RecieveCallBack = NULL;
//////////////////////////////////////////
//////////////////////////////////////////
/*
******************************************
******************************************
**      UART_INITIALIZING_FUNCTION      **
******************************************
******************************************
*/
void vUARTInit(UART_INIT_PARAM_t *uart_parameters,UARTp2F Recieve)
{
  uint16_t BRD = 0;
  if((uart_parameters->SPEED) == DOUBLE_SPEED)
  {
          BRD = (F_CPU) / (((uart_parameters->BAUD_RATE)*8UL)-1);
  }
  else if ((uart_parameters->SPEED) == NORMAL_SPEED)
  {
          BRD = (F_CPU) / (((uart_parameters->BAUD_RATE)*16UL)-1);
  }
  else //MISRA RULE
  {
	  ;
  }
  switch(uart_parameters->UART_MODE)
  {
          case ASYNCHRONOUS:
					//RX PIN DIGITAL ENABLE
					DDRD	&=~		(1U<<0);
					//TX PIN DIGITAL ENABLE
					DDRD	|=		(1U<<1);
					//BAUD-RATE-VALUE WRITING IN UBRRH AND UBRRL REGISTERS
					UBRRH	 =		(BRD>>8);
					UBRRL	 =		 BRD;
					switch(uart_parameters->BEHAVIOR_MODE)
					{
				      case POLLING:
						   //SPEED CONFIGURATION
						   UCSRA	|=		(uart_parameters->SPEED);
						   //SEND AND TRANSMITE ENABLE FOR THE UART			
						   UCSRB	|=		(1<<TXEN)|(1<<RXEN);
					       //UCSRC REGISTER SELECT AND UART CONFIGURATION
						   UCSRC	|=		(1<<URSEL)|(uart_parameters->UART_MODE)|(uart_parameters->PARITY)|(uart_parameters->STOP_BITS)|(uart_parameters->DATA_LENGTH);
                      break;
                          
					  case INTERRUPT_ON_RECEIVE:	
							//setting up the callback
							if (Recieve !=NULL)
							{
								RecieveCallBack = Recieve;
							}
							else
							{
								;
							}
                          //SPEED CONFIGURATION
                          UCSRA		|=		(uart_parameters->SPEED);
						  //SEND AND TRANSMITE ENABLE FOR THE UART
						  UCSRB		|=		(1<<TXEN)|(1<<RXEN)|(1U<<7);
						  //UCSRC REGISTER SELECT AND UART CONFIGURATION
						  UCSRC		|=		(1<<URSEL)|(uart_parameters->UART_MODE)|(uart_parameters->PARITY)|(uart_parameters->STOP_BITS)|(uart_parameters->DATA_LENGTH);
						  //GLOBAL INTERRUPT ENABLE
						  sei();
                      break;
					  
					  case INTERRUPT_ON_TRANSMIT:
					  //SPEED CONFIGURATION
					  UCSRA		|=		(uart_parameters->SPEED);
					  //SEND AND TRANSMITE ENABLE FOR THE UART
					  UCSRB		|=		(1<<TXEN)|(1<<RXEN)|(1U<<6);
					  //UCSRC REGISTER SELECT AND UART CONFIGURATION
					  UCSRC		|=		(1<<URSEL)|(uart_parameters->UART_MODE)|(uart_parameters->PARITY)|(uart_parameters->STOP_BITS)|(uart_parameters->DATA_LENGTH);
					  //GLOBAL INTERRUPT ENABLE
					  sei();
					  break;
					  
					  case INTERRUPT_ON_RECEIVE_TRANSMIT:
					  //SPEED CONFIGURATION
					  UCSRA		|=		(uart_parameters->SPEED);
					  //SEND AND TRANSMITE ENABLE FOR THE UART
					  UCSRB		|=		(1<<TXEN)|(1<<RXEN)|(1U<<6)|(1U<<7);
					  //UCSRC REGISTER SELECT AND UART CONFIGURATION
					  UCSRC		|=		(1<<URSEL)|(uart_parameters->UART_MODE)|(uart_parameters->PARITY)|(uart_parameters->STOP_BITS)|(uart_parameters->DATA_LENGTH);
					  //GLOBAL INTERRUPT ENABLE
					  sei();
					  break;
					  
					  default:
					  //MISRA RULE
                      break;
                  }
          break;
          
          case SYNCHRONOIS_MASTER_RISING_CLK_POL:
                  //RX PIN AS INPUT
                  DDRD	&=~		(1U<<0);
                  //TX PIN AS OUTPUT
                  DDRD	|=		(1U<<1);
				  //XCK PIN AS OUTPUT
				  DDRB	|=		(1U<<0);
                  //BAUD-RATE-VALUE WRITING IN UBRRH AND UBRRL REGISTERS
                  UBRRH	 =		(BRD>>8);
                  UBRRL	 =		 BRD;
                  switch(uart_parameters->BEHAVIOR_MODE)
                  {
	                  case POLLING:
	                  //SPEED CONFIGURATION
	                  UCSRA	|=		(uart_parameters->SPEED);
					  //SEND AND TRANSMITE ENABLE FOR THE UART
					  UCSRB	|=		(1<<TXEN)|(1<<RXEN);
					  //UCSRC REGISTER SELECT AND UART CONFIGURATION
					  UCSRC	|=		(1<<URSEL)|(uart_parameters->UART_MODE)|(uart_parameters->PARITY)|(uart_parameters->STOP_BITS)|(uart_parameters->DATA_LENGTH)|(0<<0);
	                  break;
	                  
	                  case INTERRUPT_ON_RECEIVE:
	                  //SPEED CONFIGURATION
	                  UCSRA		|=		(uart_parameters->SPEED);
					  //SEND AND TRANSMITE ENABLE FOR THE UART
					  UCSRB		|=		(1<<TXEN)|(1<<RXEN)|(1U<<7);
					  //UCSRC REGISTER SELECT AND UART CONFIGURATION
					  UCSRC		|=		(1<<URSEL)|(uart_parameters->UART_MODE)|(uart_parameters->PARITY)|(uart_parameters->STOP_BITS)|(uart_parameters->DATA_LENGTH)|(0<<0);
					  //GLOBAL INTERRUPT ENABLE
					  sei();
	                  break;
	                  
	                  case INTERRUPT_ON_TRANSMIT:
	                  //SPEED CONFIGURATION
	                  UCSRA		|=		(uart_parameters->SPEED);
					  //SEND AND TRANSMITE ENABLE FOR THE UART
					  UCSRB		|=		(1<<TXEN)|(1<<RXEN)|(1U<<6);
					  //UCSRC REGISTER SELECT AND UART CONFIGURATION
					  UCSRC		|=		(1<<URSEL)|(uart_parameters->UART_MODE)|(uart_parameters->PARITY)|(uart_parameters->STOP_BITS)|(uart_parameters->DATA_LENGTH)|(0<<0);
					  //GLOBAL INTERRUPT ENABLE
					  sei();
	                  break;
	                  
	                  case INTERRUPT_ON_RECEIVE_TRANSMIT:
	                  //SPEED CONFIGURATION
	                  UCSRA		|=		(uart_parameters->SPEED);
					  //SEND AND TRANSMITE ENABLE FOR THE UART
					  UCSRB		|=		(1<<TXEN)|(1<<RXEN)|(1U<<6)|(1U<<7);
					  //UCSRC REGISTER SELECT AND UART CONFIGURATION
					  UCSRC		|=		(1<<URSEL)|(uart_parameters->UART_MODE)|(uart_parameters->PARITY)|(uart_parameters->STOP_BITS)|(uart_parameters->DATA_LENGTH)|(0<<0);
					  //GLOBAL INTERRUPT ENABLE
					  sei();
	                  break;
	                  
	                  default:
	                  //MISRA RULE
	                  break;
                  }
          break;
          
          case SYNCHRONOIS_MASTER_FALLING_CLK_POL:
                  //RX PIN AS INPUT
                  DDRD	&=~		(1U<<0);
                  //TX PIN AS OUTPUT
                  DDRD	|=		(1U<<1);
                  //XCK PIN AS OUTPUT
                  DDRB	|=		(1U<<0);
                  //BAUD-RATE-VALUE WRITING IN UBRRH AND UBRRL REGISTERS
                  UBRRH	 =		(BRD>>8);
                  UBRRL	 =		 BRD;
                  switch(uart_parameters->BEHAVIOR_MODE)
                  {
	                  case POLLING:
	                  //SPEED CONFIGURATION
	                  UCSRA	|=		(uart_parameters->SPEED);
					  //SEND AND TRANSMITE ENABLE FOR THE UART
					  UCSRB	|=		(1<<TXEN)|(1<<RXEN);
					  //UCSRC REGISTER SELECT AND UART CONFIGURATION
					  UCSRC	|=		(1<<URSEL)|(uart_parameters->UART_MODE)|(uart_parameters->PARITY)|(uart_parameters->STOP_BITS)|(uart_parameters->DATA_LENGTH)|(1U<<0);
	                  break;
	                  
	                  case INTERRUPT_ON_RECEIVE:
	                  //SPEED CONFIGURATION
	                  UCSRA		|=		(uart_parameters->SPEED);
					  //SEND AND TRANSMITE ENABLE FOR THE UART
					  UCSRB		|=		(1<<TXEN)|(1<<RXEN)|(1U<<7);
					  //UCSRC REGISTER SELECT AND UART CONFIGURATION
					  UCSRC		|=		(1<<URSEL)|(uart_parameters->UART_MODE)|(uart_parameters->PARITY)|(uart_parameters->STOP_BITS)|(uart_parameters->DATA_LENGTH)|(1U<<0);
					  //GLOBAL INTERRUPT ENABLE
					  sei();
	                  break;
	                  
	                  case INTERRUPT_ON_TRANSMIT:
	                  //SPEED CONFIGURATION
	                  UCSRA		|=		(uart_parameters->SPEED);
					  //SEND AND TRANSMITE ENABLE FOR THE UART
					  UCSRB		|=		(1<<TXEN)|(1<<RXEN)|(1U<<6);
					  //UCSRC REGISTER SELECT AND UART CONFIGURATION
					  UCSRC		|=		(1<<URSEL)|(uart_parameters->UART_MODE)|(uart_parameters->PARITY)|(uart_parameters->STOP_BITS)|(uart_parameters->DATA_LENGTH)|(1U<<0);
					  //GLOBAL INTERRUPT ENABLE
					  sei();
	                  break;
	                  
	                  case INTERRUPT_ON_RECEIVE_TRANSMIT:
	                  //SPEED CONFIGURATION
	                  UCSRA		|=		(uart_parameters->SPEED);
					  //SEND AND TRANSMITE ENABLE FOR THE UART
					  UCSRB		|=		(1<<TXEN)|(1<<RXEN)|(1U<<6)|(1U<<7);
					  //UCSRC REGISTER SELECT AND UART CONFIGURATION
					  UCSRC		|=		(1<<URSEL)|(uart_parameters->UART_MODE)|(uart_parameters->PARITY)|(uart_parameters->STOP_BITS)|(uart_parameters->DATA_LENGTH)|(1U<<0);
					  //GLOBAL INTERRUPT ENABLE
					  sei();
	                  break;
	                  
	                  default:
	                  //MISRA RULE
	                  break;
                  }
          break;
		  
		  case SYNCHRONOIS_SLAVE_RISING_CLK_POL:
				  //RX PIN AS INPUT
				  DDRD	&=~		(1U<<0);
				  //TX PIN AS OUTPUT
				  DDRD	|=		(1U<<1);
				  //XCK PIN AS INPUT
				  DDRB	&=~		(1U<<0);
				  //BAUD-RATE-VALUE WRITING IN UBRRH AND UBRRL REGISTERS
				  UBRRH	 =		(BRD>>8);
				  UBRRL	 =		 BRD;
				  switch(uart_parameters->BEHAVIOR_MODE)
				  {
					  case POLLING:
					  //SPEED CONFIGURATION
					  UCSRA	|=		(uart_parameters->SPEED);
					  //SEND AND TRANSMITE ENABLE FOR THE UART
					  UCSRB	|=		(1<<TXEN)|(1<<RXEN);
					  //UCSRC REGISTER SELECT AND DATA-LENGTH AND
					  UCSRC	|=		(1<<URSEL)|(uart_parameters->UART_MODE)|(uart_parameters->PARITY)|(uart_parameters->STOP_BITS)|(uart_parameters->DATA_LENGTH)|(0<<0);
					  break;
					  
					  case INTERRUPT_ON_RECEIVE:
					  //SPEED CONFIGURATION
					  UCSRA		|=		(uart_parameters->SPEED);
					  //SEND AND TRANSMITE ENABLE FOR THE UART
					  UCSRB		|=		(1<<TXEN)|(1<<RXEN)|(1U<<7);
					  //UCSRC REGISTER SELECT AND UART CONFIGURATION
					  UCSRC		|=		(1<<URSEL)|(uart_parameters->UART_MODE)|(uart_parameters->PARITY)|(uart_parameters->STOP_BITS)|(uart_parameters->DATA_LENGTH)|(0<<0);
					  //GLOBAL INTERRUPT ENABLE
					  sei();
					  break;
					  
					  case INTERRUPT_ON_TRANSMIT:
					  //SPEED CONFIGURATION
					  UCSRA		|=		(uart_parameters->SPEED);
					  //SEND AND TRANSMITE ENABLE FOR THE UART
					  UCSRB		|=		(1<<TXEN)|(1<<RXEN)|(1U<<6);
					  //UCSRC REGISTER SELECT AND UART CONFIGURATION
					  UCSRC		|=		(1<<URSEL)|(uart_parameters->UART_MODE)|(uart_parameters->PARITY)|(uart_parameters->STOP_BITS)|(uart_parameters->DATA_LENGTH)|(0<<0);
					  //GLOBAL INTERRUPT ENABLE
					  sei();
					  break;
					  
					  case INTERRUPT_ON_RECEIVE_TRANSMIT:
					  //SPEED CONFIGURATION
					  UCSRA		|=		(uart_parameters->SPEED);
					  //SEND AND TRANSMITE ENABLE FOR THE UART
					  UCSRB		|=		(1<<TXEN)|(1<<RXEN)|(1U<<6)|(1U<<7);
					  //UCSRC REGISTER SELECT AND UART CONFIGURATION
					  UCSRC		|=		(1<<URSEL)|(uart_parameters->UART_MODE)|(uart_parameters->PARITY)|(uart_parameters->STOP_BITS)|(uart_parameters->DATA_LENGTH)|(0<<0);
					  //GLOBAL INTERRUPT ENABLE
					  sei();
					  break;
					  
					  default:
					  //MISRA RULE
					  break;
				  }
		  break;
		  
		  case SYNCHRONOIS_SLAVE_FALLING_CLK_POL:
			      //RX PIN AS INPUT
			      DDRD	&=~		(1U<<0);
			      //TX PIN AS OUTPUT
			      DDRD	|=		(1U<<1);
			      //XCK PIN AS INPUT
			      DDRB	&=~		(1U<<0);
			      //BAUD-RATE-VALUE WRITING IN UBRRH AND UBRRL REGISTERS
			      UBRRH	 =		(BRD>>8);
			      UBRRL	 =		 BRD;
			      switch(uart_parameters->BEHAVIOR_MODE)
			      {
				      case POLLING:
				      //SPEED CONFIGURATION
				      UCSRA	|=		(uart_parameters->SPEED);
					  //SEND AND TRANSMITE ENABLE FOR THE UART
					  UCSRB	|=		(1<<TXEN)|(1<<RXEN);
					  //UCSRC REGISTER SELECT AND UART CONFIGURATION
					  UCSRC	|=		(1<<URSEL)|(uart_parameters->UART_MODE)|(uart_parameters->PARITY)|(uart_parameters->STOP_BITS)|(uart_parameters->DATA_LENGTH)|(1U<<0);
				      break;
				      
				      case INTERRUPT_ON_RECEIVE:
				      //SPEED CONFIGURATION
				      UCSRA		|=		(uart_parameters->SPEED);
					  //SEND AND TRANSMITE ENABLE FOR THE UART
					  UCSRB		|=		(1<<TXEN)|(1<<RXEN)|(1U<<7);
					  //UCSRC REGISTER SELECT AND UART CONFIGURATION
					  UCSRC		|=		(1<<URSEL)|(uart_parameters->UART_MODE)|(uart_parameters->PARITY)|(uart_parameters->STOP_BITS)|(uart_parameters->DATA_LENGTH)|(1U<<0);
					  //GLOBAL INTERRUPT ENABLE
					  sei();
				      break;
				      
				      case INTERRUPT_ON_TRANSMIT:
				      //SPEED CONFIGURATION
				      UCSRA		|=		(uart_parameters->SPEED);
					  //SEND AND TRANSMITE ENABLE FOR THE UART
					  UCSRB		|=		(1<<TXEN)|(1<<RXEN)|(1U<<6);
					  //UCSRC REGISTER SELECT AND UART CONFIGURATION
					  UCSRC		|=		(1<<URSEL)|(uart_parameters->UART_MODE)|(uart_parameters->PARITY)|(uart_parameters->STOP_BITS)|(uart_parameters->DATA_LENGTH)|(1U<<0);
					  //GLOBAL INTERRUPT ENABLE
					  sei();
				      break;
				      
				      case INTERRUPT_ON_RECEIVE_TRANSMIT:
				      //SPEED CONFIGURATION
				      UCSRA		|=		(uart_parameters->SPEED);
					  //SEND AND TRANSMITE ENABLE FOR THE UART
					  UCSRB		|=		(1<<TXEN)|(1<<RXEN)|(1U<<6)|(1U<<7);
					  //UCSRC REGISTER SELECT AND UART CONFIGURATION
					  UCSRC		|=		(1<<URSEL)|(uart_parameters->UART_MODE)|(uart_parameters->PARITY)|(uart_parameters->STOP_BITS)|(uart_parameters->DATA_LENGTH)|(1U<<0);
					  //GLOBAL INTERRUPT ENABLE
					  sei();
				      break;
				      
				      default:
				      //MISRA RULE
				      break;
			      }
		  break;

          default:
		  //MISRA RULE
          break;
          
  }
}
//////////////////////////////////////////
//////////////////////////////////////////
/*
******************************************
******************************************
**       UART_RECIEVING_FUNCTION        **
******************************************
******************************************
*/
uint8_t u8tUARTRx(void)
{
   while ( !(UCSRA & (1<<RXC)) );
   return UDR;
}	
//////////////////////////////////////////
//////////////////////////////////////////
/*
******************************************
******************************************
**   UART_BYTE_TRANSMITTING_FUNCTION    **
******************************************
******************************************
*/
void vUARTTxByte(uint8_t data)
{
  while ( !(UCSRA & (1<<UDRE)) );
  UDR = data;
}
//////////////////////////////////////////
//////////////////////////////////////////
/*
******************************************
******************************************
**  UART_STRING_TRANSMITTING_FUNCTION   **
******************************************
******************************************
*/
void vUARTTxString(char string[])
{
   while (*string != '\0')
  {
    vUARTTxByte(*(string++));
  }
}
//////////////////////////////////////////
//////////////////////////////////////////
/*
******************************************
******************************************
**       UART_INTERRUPT_FUNCTIONS       **
******************************************
******************************************
*/
#if (BEHAVIOR_MODE == INTERRUPT_ON_RECEIVE)
ISR(USART_RXC_vect)
{
	RecieveCallBack();
}
#endif

#if (BEHAVIOR_MODE == INTERRUPT_ON_TRANSMIT)
ISR(USART_TXC_vect)
{
	DDRC  |= (1U<<0);
	PORTC ^= (1U<<0);
}
#endif
//////////////////////////////////////////
//////////////////////////////////////////
